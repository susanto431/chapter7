const { Article } = require('../models')

module.exports = {
    createArticle: (req, res) => {
        const { title, body } = req.body
        if (title === undefined || body === undefined) {
            return res.status(400).json("Data tidak lengkap")
        }
        Article.create({
            title: title,
            body: body,
            approved: false
        }).then(article => {
            res.status(200).json("Berhasil membuat artikel")
        }).catch(err => {
            res.status(500).json('Internet Server problem')
        })
    },

    findAll: (req, res) => {
        Article.findAll().then(article => {
            res.render('articles/index', { article })
        })
    },

    findOne: (req, res) => {
        Article.findOne({
            where: { id: req.params.id }
        }).then(articles => {
            res.render('articles/show', { articles })
        })
    },

    edit: (req, res) => {

        Article.findOne({
            where: { id: req.params.id }
        }).then(articles => {
            res.render('articles/update', { articles })
        })
    },

    update: (req, res) => {
        const { title, body } = req.body
        if (title === undefined || body === undefined) {
            return res.status(400).json("Data tidak lengkap")
        }
        Article.update({
            title: title,
            body: body,
            update: true
        }, {
            where: { id: req.params.id }
        }).then(articles => {
            res.render('articles/index', { articles })
        }).catch(err => {
            res.status(500).json("Internet Server Problem")
        })
    }

}