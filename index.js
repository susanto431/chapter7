const express = require('express');
const app = express();

const articleRouter = require('./router/articleRouters')

app.use(express.json())
app.use(express.urlencoded({
    extented: false
}))
app.set('view engine', 'ejs')
app.use(articleRouter);


app.listen(3000, () => {
    console.log(`Server started on port`);
});




// app.use(express.json());


// app.get('/api/article', (req, res) => {
//     Article.findAll().then(article => {
//         res.status(200).json(article)
//     })
// });

// app.get('/api/article/:idArticle', (req, res) => {
//     Article.findOne({
//         where: { id: req.params.idArticle }
//     }).then(articles => { res.status(200).json(articles) })
// });

// app.post('/api/article', (req, res) => {
//     const { title, body, approved } = req.body

//     if (title === undefined || body === undefined || approved === undefined) {
//         return res.status(400).json("Salah satu field kosong!")
//     }


//     Article.create({
//         title: req.body.title,
//         body: req.body.body,
//         approved: req.body.approved
//     }).then(articles => {
//         res.status(200).json(articles)
//     }).catch(err => {
//         res.status(500).json("Internal Server Error")
//     })
// });

// app.put('/api/article/:idArticle', (req, res) => {
//     const { title, body, approved } = req.body

//     if (title === undefined || body === undefined || approved === undefined) {
//         return res.status(400).json("Salah satu field kosong!")
//     }

//     const query = {
//         where: { id: req.params.idArticle }
//     }


//     Article.update({
//         title: title,
//         body: body,
//         approved: approved
//     }, query).then(articles => {
//         res.status(200).json(articles)
//     }).catch(err => {
//         res.status(401).json("Update gagal")
//     })
// });

// app.delete('/api/article/:idArticle', (req, res) => {
//     const query = {
//         where: { id: req.params.idArticle }
//     }
//     Article.destroy(
//         //{ where: { id: req.params.idArticle }  }
//         query
//     ).then(articles => {
//         res.status(200).json(articles)
//     }).catch(err => {
//         res.status(500).json("Internet server problem")
//     })
// });