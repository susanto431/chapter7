const express = require('express');
const articleControllers = require('../controllers/articleControllers');
const articleRouter = express.Router()
const ArticleController = require('../controllers/articleControllers')

articleRouter.get('/article/create', (req, res) => {
    res.render('articles/create')
    // pada render ini, ditulis nama folder, dan nama filenya. jd lebh spesifik.

})

articleRouter.post('/api/article', articleControllers.createArticle)
articleRouter.get('/api/articles', articleControllers.findAll)
articleRouter.get('/show/articles/:id', articleControllers.findOne);
articleRouter.get('/edit/articles/:id', articleControllers.edit);
articleRouter.put('/update/articles/:id', articleControllers.update);

module.exports = articleRouter


